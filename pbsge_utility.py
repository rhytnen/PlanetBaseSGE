import copy
import os
import sys
import xml.etree.ElementTree as ET
from random import randint

#  TODO:  redo this c-style enumeration
RESOURCE_MEAL = 0
RESOURCE_METAL = 1
RESOURCE_BIOPLASTIC = 2

resource_type = {RESOURCE_MEAL: ('Meal', ('subtype', 'value', '13')),
                 RESOURCE_METAL: ('Metal', ('subtype', 'value', '0')),
                 RESOURCE_BIOPLASTIC: ('Bioplastic', ('subtype', 'value', '0'))}

COLONIST_ENGINEER = 0
COLONIST_BIOLOGIST = 1
COLONIST_WORKER = 2
COLONIST_GUARD = 3
COLONIST_DOCTOR = 4

colonist_type = {COLONIST_ENGINEER: ('Engineer', 'False'), COLONIST_BIOLOGIST: ('Biologist', 'False'),
                 COLONIST_WORKER: ('Worker', 'False'), COLONIST_GUARD: ('Guard', 'False'),
                 COLONIST_DOCTOR: ('Biologist', 'True')}

BOT_CARRIER = 1
BOT_CONSTRUCTOR = 2
BOT_DRILLER = 3


def fill_power_collectors(constructions):
    """
    Locations power-storage blocks in constructions and sets its value
    :param constructions: xml blocks of constructions
    """
    collectors = constructions.iter('power-storage')
    for collector in collectors:
        collector.set('value', '1.25E+07')


def make_generic_colonist(next_id):
    """
    Makes the basic template for all colonists.  No specialization is given here.

    :param next_id: a unique, sequential, integer identifier for all objects in the game
    :return: xml block representing a colonist
    """
    c = ET.Element('character')
    c.set('type', 'Colonist')
    ET.SubElement(c, 'location', {'value': '0'})
    ET.SubElement(c, 'name', {'value': 'First Last'})
    ET.SubElement(c, 'status-flags', {'value': '0'})
    ET.SubElement(c, 'state', {'value': '0'})
    ET.SubElement(c, 'id', {'value': str(next_id)})
    ET.SubElement(c, 'wander-time', {'value': '5'})
    ET.SubElement(c, 'Health', {'value': '1'})
    ET.SubElement(c, 'Nutrition', {'value': '1'})
    ET.SubElement(c, 'Hydration', {'value': '1'})
    ET.SubElement(c, 'Morale', {'value': '1'})
    ET.SubElement(c, 'Sleep', {'value': '1'})
    ET.SubElement(c, 'Oxygen', {'value': '1'})
    ET.SubElement(c, 'gender', {'value': '1'})
    ET.SubElement(c, 'basic-meal-count', {'value': '0'})
    ET.SubElement(c, 'head-index', {'value': str(randint(0, 4))})
    ET.SubElement(c, 'skin-color-index', {'value': str(randint(0, 6))})
    ET.SubElement(c, 'inmunity-to-contagion-time', {'value': '0'})
    return c


def make_colonist(next_id, colonist, pos):
    """
    Creates a conolist of type colonist
    :param next_id: unique identifer
    :param colonist: a colonist_type key
    :param pos: a place to locate him in game
    :return: xml character block for a colonist
    """
    cpos = copy.copy(pos)
    if colonist in colonist_type:
        c = make_generic_colonist(next_id)
        ct = colonist_type[colonist]
        ET.SubElement(c, 'specialization', {'value': ct[0]})
        ET.dump(c)
        ET.SubElement(c, 'doctor', {'value': ct[1]})
        c.append(cpos)
        return c


def complete_constructions(constructions):
    """
    Loops through constructions and components setting their values to complete
    :param constructions: xml blocks of constructions
    """
    for construction in constructions.findall('construction'):
        if construction is None:
            continue
        module_name = construction.find('module-type')
        if module_name is None:
            module_name = "Connection"
        else:
            module_name = module_name.get('value')

        build = construction.find('build-progress')
        if build.get('value') != '-1':
            build.set('value', '-1')
            print "Completing Construction On:", module_name
        construction.find('state').set('value', '3')
        construction.find('condition').set('value', '1')
        materials = construction.find('construction-materials')
        if materials is not None:
            construction.remove(materials)
            print " -- removed materials"
        pending = construction.find('pending-construction-costs')
        if pending is not None:
            construction.remove(pending)
            print " -- removed pending"

        components = construction.find('components')
        if components is None:
            continue
        for component in components.findall('component'):
            component_name = component.find('component-type').get('value')
            build = component.find('build-progress')
            if build.get('value') == '-1':
                continue
            build.set('value', '-1')
            print "  Completing component:", component_name
            component.find('state').set('value', '3')
            component.find('condition').set('value', '1')
            materials = component.find('construction-materials')
            if materials is not None:
                component.remove(materials)
                print "   -- removed materials"
            pending = component.find('pending-construction-costs')
            if pending is not None:
                component.remove(pending)
                print "   -- removed pending"


def make_generic_resource(next_id):
    """
    creates a generic resource block without fields identifying its type
    :param next_id: unique identifer
    :return: xml element for a resource
    """
    r = ET.Element('resource')
    ET.SubElement(r, 'id', {'value': str(next_id)})
    ET.SubElement(r, 'trader-id', {'value': '-1'})
    ET.SubElement(r, 'state', {'value': '0'})
    ET.SubElement(r, 'location', {'value': '0'})
    ET.SubElement(r, 'condition', {'value': '1'})
    ET.SubElement(r, 'durability', {'value': '0'})

    return r


def make_resource(next_id, resource):
    """
    :param: next_id unique identifier
    :param: resource type as defined in resource_type
    :rtype: object
    """
    if resource in resource_type:
        r = make_generic_resource(next_id)
        rt = resource_type[resource]
        r.set('type', rt[0])
        for st in rt[1:]:
            ET.SubElement(r, st[0], {st[1]: st[2]})

    return r


def refresh_characters(characters):
    """
    Sets the various stats of characters to 1 (full)
    :param characters: the character xml element to loop over
    :return: the character xml element
    """
    if characters is None:
        return

    stats = ('Health', 'Nutrition', 'Hydration', 'Oxygen', 'Sleep', 'Morale', 'Condition')
    for stat in stats:
        for x in characters.iter(stat):
            x.set('value', '1')
    return characters


def enable_tech():
    """
    Creates a tech xml element with all tech enabled
    :return: the tech xml element
    """
    tech_types = (
        "TechDrillerBot", "TechGoliathTurbine", "TechConstructorBot", "TechColossalPanel", "TechFarmDome",
        "TechGMTomatoes",
        "TechGmOnions", "TechMassiveStorage")
    tech = ET.Element('techs')
    for t in tech_types:
        new_t = ET.SubElement(tech, 'tech')
        new_t.set('value', t)
    return tech


def fill_storage(storage, resources, resource_type, count, next_id):
    # height appears to be .45
    """
    Creates a number of resources and places them in storage or outside of airlock
    :param storage: xml element containing the available storage in game
    :param resources: xml element containing available resources
    :param resource_type: type of element to create
    :param count: number of elements to create
    :param next_id: unique identifier
    :return: the new next_id and number of items placed

    TODO:  Rewrite airlock placement based on orientation
    """
    box_height = .45  # magic number
    items_placed = 0

    rs = storage
    for slot in rs.findall('slot'):
        position = slot.find('position')
        xpos = position.get('x')
        zpos = position.get('x')

        max_height = float(slot.find('max-height').get('value'))
        cur_height = float(position.get('y'))
        for res in slot.findall('resource'):
            res_pos = res.find('position')
            cur_height = float(res_pos.get('y'))
        while cur_height + box_height < max_height:
            if items_placed >= count:
                break;
            cur_height += box_height
            next_id += 1
            items_placed += 1
            res = make_resource(next_id, resource_type)
            ET.SubElement(res, 'position', dict(x=xpos, y=str(cur_height), z=zpos))
            ET.SubElement(res, 'orientation', dict(x='0', y='2', z='0'))
            slot.append(res)
            # new_res = copy.copy(res)
            # resources.append(res)

    return next_id, items_placed


if __name__ == "__main__":

    user_path = os.environ['HOMEPATH']
    pb_path = '\\Documents\\Planetbase\\Saves\\'
    file_name = sys.argv[1]
    file_path = user_path + pb_path + file_name

    if os.path.isfile(file_path) is False:
        print "Error: Couldn't find file:", file_path
        exit()

    # the following is an example of use but will be replaced by the UI
    tree = ET.parse(file_path)
    root = tree.getroot()

    idGenerator = root.find('id-generator')
    next_id = int(idGenerator.find('next-id').get('value'))
    next_bot_id = idGenerator.find('next-bot-id').get('value')

    # tech
    techs = root.find('techs')
    techs = enable_tech()

    # char buffs
    characters = refresh_characters(root.find('characters'))

    resources = root.find('resources')
    constructions = root.find('constructions')
    characters = root.find('characters')

    complete_constructions(constructions)
    fill_power_collectors(constructions)

    res_storage = root.findall('./constructions/construction/resource-storage')
    cons = constructions.findall('construction')
    pos = None
    for con in cons:
        if con.get('type') != 'Module':
            continue

        modtype = con.find('module-type').get('value')
        if modtype == 'ModuleTypeAirlock':
            pos = con.find('position')

    for i in range(0, 5):
        c = make_colonist(next_id, COLONIST_BIOLOGIST, pos)
        characters.append(c)
        next_id += 1
    for i in range(0, 5):
        c = make_colonist(next_id, COLONIST_ENGINEER, pos)
        characters.append(c)
        next_id += 1
    for i in range(0, 2):
        c = make_colonist(next_id, COLONIST_GUARD, pos)
        characters.append(c)
        next_id += 1

    if res_storage is not None:
        next_id, items_placed = fill_storage(res_storage[0], resources, RESOURCE_MEAL, 100, next_id)
        print "Placed", items_placed, "items."
        print "New Next_ID = ", next_id

    idGenerator.find('next-id').set('value', str(next_id))

    print "Writing to:", file_path + ".mod"
    tree.write(file_path + ".mod")
